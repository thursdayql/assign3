
public class PairOfDice
{
  private final int MAX = 6;
  private int firstDie;
  private int secondDie;
  
  // Constructor
  public PairOfDice()
  {
    firstDie = 1;
    secondDie = 1;
  }
  
  // Rolls for value and returns
  public void roll()
  {
    firstDie = (int)(Math.random() * MAX) + 1;
    secondDie = (int)(Math.random() * MAX) + 1;
  }
  
  public int getFirstDie()
  {
    return firstDie;
  }
  
  public int getSecondDie()
  {
    return secondDie;
  }
  
  public String toString()
  {
    String toStr = "";
    toStr += " Face Value of first die: " + firstDie;
    toStr += "\n Face Value of second die: " + secondDie;
    
    return toStr;
  }
  
  public int getTotal()
  {
    return firstDie + secondDie;
  }  
}
