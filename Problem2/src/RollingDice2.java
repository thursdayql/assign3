//********************************************************************
//File:         RollingDice2.java       
//Author:       Liam Quinn
//Date:         Oct 10, 2017
//Course:       CPS100
//
//Problem Statement:
// Using the Die class defined in this chapter, write a class called
// PairOfDice, composed of two Die objects. Include methods to set and get
// the individual die values, a method to roll the dice, and a method
// that returns the current sum of the two die values.
// Create a driver class called RollingDice2 to instantiate and use a 
// PairOfDice object.
//
//Inputs: None 
//Outputs:  Results of testing all PairOfDice's methods
// 
//********************************************************************

public class RollingDice2
{

  public static void main(String[] args)
  { 
    PairOfDice Die = new PairOfDice();
    
    Die.roll();
    
    System.out.println(Die);
    System.out.println(" Sum of both face values: " + Die.getTotal());
  }
}
