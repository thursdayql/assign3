
public class Book
{
  private String bookTitle;
  private String bookAuthor;
  private String bookPublisher;
  private String publishDate;

  // Book Constructor
  public Book(String title, String author, String publisher, String date)
  {
    bookTitle = title;
    bookAuthor = author;
    bookPublisher = publisher;
    publishDate = date;
  }

  // Book Setter
  public void setBookName(String bookName)
  {
    bookTitle = bookName;
  }

  // Book Getter
  public String getBookName()
  {
    return bookTitle;
  }

  // toString
  public String toString()
  {
    String toStr = "";
    toStr += " Book Title: " + bookTitle;
    toStr += "\n Author: " + bookAuthor;
    toStr += "\n Publisher: " + bookPublisher;
    toStr += "\n Copyright and Publish Date: " + publishDate;

    return toStr;
  }
}

