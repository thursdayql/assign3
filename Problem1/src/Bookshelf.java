
//********************************************************************
//File:         Bookshelf.java       
//Author:       Liam Quinn
//Date:         Oct 10, 2017
//Course:       CPS100
//
//Problem Statement:
// Write a class called book that contains instance data for the title,
// author, publisher, and copyright date. Define the book constructor to
// accept and initialize this data. Include setter and getter methods for
// all instance data. Include a toString method that returns a nicely
// formatted, multi-line description of the book.
// Create a driver class called bookshelf, whose main method
// instantiates and updates several book objects.
//
//Inputs: None 
//Outputs:  Results of testing all Book's methods
// 
//********************************************************************

public class Bookshelf
{

  public static void main(String[] args)
  {

    Book bookIt = new Book("It", "Stephen King", "Viking",
        "September 15, 1986");
    Book bookHungerGames = new Book("Hunger Games", "Suzanne Collins",
        "Scholastic Press", "September 14, 2008");

    System.out.println(bookIt);
    System.out.println("");
    System.out.println(bookHungerGames);
    System.out.println("");

    System.out.println("");
    System.out
        .println("The program will now set a new name for the book title's");
    System.out.println("");

    // Use of Book Title Setter
    System.out.println("");
    bookIt.setBookName("The Shining");
    System.out.println(bookIt);
    System.out.println("");
    bookHungerGames.setBookName("Mockingjay");
    System.out.println(bookHungerGames);
  }

}
